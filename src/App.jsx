import React, { useState } from 'react';

import Player from './components/Player';
import Thumbnail from './components/Thumbnail';
import teliaData from './teliatestdata.json';


function App() {
	const [activeBox, setActiveBox] = useState(0);

	return (
		<div className="container App pt-5">
			<Player assetInfo={teliaData[activeBox]} />
			<div className="thumbnails-container row mt-3 justify-content-center">
				{
					teliaData.map((item, i) => <Thumbnail key={item.id} index={i} info={item} active={activeBox === i ? true : false} activeBoxFn={setActiveBox} />)
				}
			</div>
		</div>
	);
}

export default App;

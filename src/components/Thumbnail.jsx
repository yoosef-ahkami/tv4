import React from 'react';

function Thumbnail(props) {
    const { description, image, name } = props.info;
    return (
        <div className={`col-xs-1 col-sm-6 col-md-4 col-lg  thumbnail-box p-2 mb-3 ${props.active ? 'active bg-light shadow' : ''}`} onClick={() => props.activeBoxFn(props.index)}>
            <div className={`img-box rounded mx-auto d-block`} style={{ backgroundImage: `url(${image})` }}>
                <div className="desc">{description}</div>
            </div>
            <div className="name">{name}</div>
        </div>
    )
}

export default Thumbnail

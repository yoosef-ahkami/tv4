import React, { useEffect, useState } from 'react';
import shaka from 'shaka-player';

let timeJump = 10;
let videoElement;
let shakaPlayer;

function Player(props) {
    const [playerStatus, setPlayerStatus] = useState('play');
    const [errorCode, setErrorCode] = useState();
    const [progress, setProgress] = useState(0);

    const { name, drmType, streamingURL, drmHeaders, drmLicenseServer } = props.assetInfo;

    useEffect(() => {
        if (videoElement && shakaPlayer) {
            deInit();
        }
        initApp();

    }, [streamingURL]);

    const shakaConfig = () => {
        if (!shakaPlayer) {
            return;
        }
        shakaPlayer.configure({
            drm: {
                servers: {
                    'com.widevine.alpha': drmLicenseServer,
                }
            },
        });

        shakaPlayer.getNetworkingEngine().registerRequestFilter((type, request) => {
            if (type === shaka.net.NetworkingEngine.RequestType.LICENSE) {
                request.headers['X-AxDRM-Message'] = drmHeaders['X-AxDRM-Message'];
            }
        });
    };

    const initApp = () => {
        shaka.polyfill.installAll();
        if (shaka.Player.isBrowserSupported()) {
            initPlayer().then(() => videoElement.play());
        } else {
            console.error('Browser not supported!');
        }
    };

    const setupVideoElement = () => {
        const videoContainer = document.getElementById('video-box');
        videoElement = window.document.createElement('video');
        videoElement.setAttribute("video-element", "Div1");
        videoElement.setAttribute("controls", true);
        videoContainer.appendChild(videoElement);

        videoElement.addEventListener('timeupdate', () => progressControl());
        videoElement.addEventListener('seeked', () => setPlayerStatus('pause'));
        videoElement.addEventListener('playing', () => setPlayerStatus('pause'));
        videoElement.addEventListener('play', () => setPlayerStatus('pause'));
        videoElement.addEventListener('pause', () => setPlayerStatus('play'));

    }

    const initPlayer = async () => {
        setupVideoElement();

        shakaPlayer = new shaka.Player(videoElement);
        window.shaka = shakaPlayer;
        shakaPlayer.addEventListener('error', onErrorEvent);

        if (drmType !== 'NONE') {
            shakaConfig();
        }

        try {
            await shakaPlayer.load(streamingURL);
            setErrorCode(null);
            console.log('The video has now been loaded!');
        } catch (e) {
            onError(e);
        }
    };

    const playbackControl = () => {
        if (!videoElement) {
            return;
        }

        if (playerStatus === 'play') {
            setPlayerStatus('pause');
            videoElement.play();
        } else {
            setPlayerStatus('pause');
            videoElement.pause();
        }
    };

    const timeControl = type => {
        const duration = videoElement.duration;
        const currentTime = videoElement.currentTime;

        switch (type) {
            case 'rwd':
                if (currentTime >= timeJump) {
                    videoElement.currentTime = currentTime - timeJump;
                }
                break;

            default:
                if (currentTime + timeJump <= duration) {
                    videoElement.currentTime = currentTime + timeJump;
                }
                break;
        }
    };

    const progressControl = () => {
        const progressInPercent = (videoElement.currentTime * 100) / videoElement.duration;
        setProgress(progressInPercent);
    };

    const onErrorEvent = event => {
        onError(event.detail);
    };

    const onError = error => {
        setErrorCode(error.code);
        console.error('Error code', error.code, 'object', error);
    };


    const deInit = () => {
        const videoContainer = document.getElementById('video-box');
        if (shakaPlayer) {
            shakaPlayer.detach();
            shakaPlayer.unload();
            shakaPlayer = null;
        }

        if (videoElement) {
            videoContainer.removeChild(videoElement);
            videoElement = null;
        }
    };

    return (
        <div className={`player-box`}>
            <h3 className="title-box text-center mb-3">{name}</h3>
            <div id="video-box" className="ratio ratio-16x9"></div>
            <div className="control-box">
                <div className="btn-controls-box row mt-3">
                    <div className="btn-group col-sm-12 col-md-4 col-xl-2">
                        <button type="button" className="btn btn-outline-primary" onClick={() => timeControl('rwd')}>{`<<<`}</button>
                        <button type="button" className="btn btn-outline-primary" onClick={playbackControl}>{playerStatus}</button>
                        <button type="button" className="btn btn-outline-primary" onClick={() => timeControl('fwd')}>{`>>>`}</button>
                    </div>
                    <div className="col-sm-12 col-md-8 col-xl-10">
                        <div className="progress mt-2">
                            <div className="progress-bar" role="progressbar" style={{ width: `${progress}%` }} aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            {
                (errorCode === 4032 || errorCode === 6001) &&
                <div className="alert alert-danger mt-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                    </svg>
                    The content container or codecs are not supported by this browser.
                </div>
            }
        </div>
    )
}

export default Player
